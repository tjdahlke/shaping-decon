#!/bin/bash

__create_user() {
# Create a user to SSH into as.
useradd --create-home user --shell /bin/bash
SSH_USERPASS=newpass
mkdir /var/run/sshd
chmod 0755 /var/run/sshd
echo -e "user:$SSH_USERPASS" | chpasswd
echo ssh user password: $SSH_USERPASS
echo datapath=stdout > /home/user/.datapath
}

# Call all functions
__create_user
