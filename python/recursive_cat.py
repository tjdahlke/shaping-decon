#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#
#   PROGRAM THAT DOES A RECURSIVE CAT
#
#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


#!/usr/local/bin/python
import sys
import os
import math
import commands

# Read the inputs
if len(sys.argv) < 3:
    print('Error: Not enough input args')
    sys.exit()
threshold = int(sys.argv[1])
outputFileName = sys.argv[2]
lsSearchTerm = sys.argv[3]

# Search for the files using the ls argument. We do this because python has a limit on the number of input arguments. This varies with the OS you're using.
searchForFiles="ls %s" % (lsSearchTerm)
junk,out1=commands.getstatusoutput(searchForFiles)
inputfiles = out1.split()

# Count number of files
# searchForFiles="ls %s | wc -l" % (lsSearchTerm)
# junk,nfiles1=commands.getstatusoutput(searchForFiles)
# nfiles2=len(inputfiles)

# Initialize
sublist=[]
ListOfVirtualCats=[]
checklist=[]
subCount=0
bigCount=0
allFileCount=1

# Make virtual sub-Cats of subsets of the files
for thisFile in inputfiles:

    if((subCount==threshold) or (allFileCount==len(inputfiles))):
        # Add the last file
        sublist.append(thisFile)
        subCount=subCount+1

        # Cat the files together
        checklist=checklist+sublist
        smallListOfFiles = ' '.join(map(str, sublist))
        bigfilename="%s%s" % (outputFileName,bigCount)
        cmd = "/usr/local/SEP/SEP8/bin/Cat3d max_memory=10000000000 axis=2 virtual=1 %s hff=%s@@ > %s" % (smallListOfFiles,bigfilename,bigfilename)
        print("==============================================")
        print(cmd)
        commands.getoutput(cmd)
        ListOfVirtualCats.append(bigfilename)
        bigCount=bigCount+1

        # Zero the list and count
        subCount=0
        sublist=[]

    else:
        sublist.append(thisFile)
        subCount=subCount+1
    allFileCount=allFileCount+1


# Do the final cat of the virtual sub-Cats
StringOfVirtualCats = ' '.join(map(str, ListOfVirtualCats))
final="/usr/local/SEP/SEP8/bin/Cat3d max_memory=100000000 axis=2 virtual=0 %s hff=%s@@ > %s" % (StringOfVirtualCats,outputFileName,outputFileName)
print(final)
commands.getoutput(final)


