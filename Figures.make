
fignode=3

#==============================================================================

ER: ${R}/synWavelet.pdf ${R}/Node${fignode}StackedWavelet.pdf ${R}/AverageStackedWavelet.pdf ${R}/UncorrectedNear.pdf ${R}/HMOcorrected.pdf

NR:

CR: ${R}/ShapedDOWNData.pdf ${R}/ShapedUPData.pdf ${R}/ShapedTraceCompare.pdf

#==============================================================================


${R}/synWavelet.v: matching_wavelet.H
	Cp $< $@1
	echo "o1=-0.5" >> $@1
	Window3d min1=0.0 max1=2.0 < $@1 | Graph title=" " label1='Time[s]' label2='Amplitude' ${FIG}$@
	rm $@1

${R}/UncorrectedNear.v: 2Dline${fignode}.H.near.binned
	Grey title=" " label1='Time[s]' label2='Source X position [m]'  < $< ${FIG}$@

${R}/HMOcorrected.v: 2Dline${fignode}.H.near.HMO.binned
	Grey title=" " label1='Time[s]' label2='Source X position [m]'  < $< ${FIG}$@

${R}/Node${fignode}StackedWavelet.v: 2Dline${fignode}.H.near.HMO.binned.stacked
	Graph max1=2.0 title=" " label1='Time[s]' label2='Amplitude' < $< ${FIG}$@

${R}/AverageStackedWavelet.v: average_bubble.H
	Graph max1=2.0 title=" " label1='Time[s]' label2='Amplitude' < $< ${FIG}$@

${R}/ShapedDOWNData.v: 2Dline${fignode}.H.shaped.binned
	Grey max1=6.0 title=" " label1='Time[s]' label2='Source X position [m]' < $< ${FIG}$@   

${R}/ShapedUPData.v: 2Dline${fignode}.UP.shaped.binned
	Grey max1=6.0 title=" " label1='Time[s]' label2='Source X position [m]' < $< ${FIG}$@

${R}/ShapedTraceCompare.v: 2Dline${fignode}.UP.shaped 2Dline${fignode}.UP
	Window3d synch=1 f2=100 n2=1 < 2Dline${fignode}.UP.shaped > $@1 hff=-1 
	Window3d synch=1 f2=100 n2=1 < 2Dline${fignode}.UP            > $@2 hff=-1
	Scale < $@1 > $@11
	Scale < $@2 > $@22
	Cat3d axis=2 $@11 $@22 | Graph max1=2.0 title=" " label1='Time[s]' label2='Amplitude' ${FIG}$@
	rm $@1 $@2 $@11 $@22

# ${R}/Uncorrected.v: 2Dline${fignode}.H.binned
# 	Grey title=" " label1='Time[s]' label2='Source X position [m]'  < $< ${FIG}$@

# ${R}/Node155StackedWavelet.v: 2Dline155.H.HMO.stacked
# 	Graph max1=2.0 title=" " label1='Time[s]' label2='Amplitude' < $< ${FIG}$@

# ${R}/ShapedData.v: 2Dline${fignode}.H.shaped.binned
# 	Grey max1=6.0 title=" " label1='Time[s]' label2='Source X position [m]' < $< ${FIG}$@

#===================================================================================

%.pdf: %.v
	pstexpen $< $@1 color=y fat=1 fatmult=1.5 invras=y
	epstopdf $@1
	rm $@1 
