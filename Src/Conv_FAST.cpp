#include <iostream>
#include "float1DReg.h"
#include "ioModes.h"
#include <string>
#include "Signal1D.h"


using namespace SEP;

int main(int argc, char **argv) {

	// Initialize the IO
	ioModes modes(argc,argv); 
	std::shared_ptr <genericIO> ptrIo  = modes.getDefaultIO();
	std::shared_ptr <paramObj>  ptrPar = ptrIo->getParamObj(); 	
	std::shared_ptr <genericRegFile> ptrData = ptrIo->getRegFile("in",usageIn);	
	std::shared_ptr <genericRegFile> ptrOut = ptrIo->getRegFile("out",usageOut);
	std::shared_ptr <genericRegFile> ptrFil = ptrIo->getRegFile("filter",usageIn);	

	// Initialize the data arrays
	std::shared_ptr <hypercube> hypData = ptrData->getHyper();

	// Get ntraces to loop over
	std::vector<int> inputN=hypData->getNs();
	int nt=inputN[0];
	int ntraces=inputN[1];
    SEP::axis axT = hypData->getAxis(1);

	std::shared_ptr<hypercube> hyperTrace(new hypercube(axT));

	// Allocate the single trace
	std::shared_ptr <Signal1D> data(new Signal1D(hyperTrace));
	std::shared_ptr <Signal1D> out(new Signal1D(hyperTrace));

	// Read in the filter
	std::shared_ptr <hypercube> hypFil = ptrFil->getHyper();
	std::shared_ptr <float1DReg> filter(new float1DReg(hypFil));
	ptrFil->readFloatStream(filter);

	// Set the output description
	ptrOut->setHyper(hypData); 	// Copy the pointer to the hypercube object
	ptrOut->writeDescription(); // write description on SEP header file 

	// Start loop
    for(int itrace=0; itrace < ntraces; itrace++) {

    		fprintf(stderr, "trace # = %d\n",100*itrace/ntraces );
			// Read data
			ptrData->readFloatStream(data->getVals(),hyperTrace->getN123());

			// Do convolution
			out = data->conv(filter);

			// Write the data out
			ptrOut->writeFloatStream(out->getVals(),hyperTrace->getN123());	

	}

	return 0;
}