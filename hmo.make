################################################################################################################
# This makefile contains the rules for bubble estimation using HMO


nfake=2
maxoff=1000
poslag=400
neglag=20

################## Extract a sourceline projecting nodes at determined offset ##############################

downgoing_data%.H:
	make --directory=../pz-summation ${main}/pz-summation/DOWN/down$*.H
	Cp ${main}/pz-summation/DOWN/down$*.H $@

2Dline%.H: downgoing_data%.H
	Window_key < $< synch=1 nkeys=1 key1=nominalshotline \
	mink1=1486 maxk1=1486 > $@1 hff=$@1@@
	Window3d max1=6.0 < $@1 > $@2
	Headermath maxsize=100000 \
	key1=offx eqn1='rx-sx' \
	< $@2 > $@ hff=$@@@;
	rm $@1 $@2

upgoing_data%.H:
	make --directory=../pz-summation ${main}/pz-summation/UP/up$*.H
	Cp ${main}/pz-summation/UP/up$*.H $@

2Dline%.UP: upgoing_data%.H
	Window_key < $< synch=1 nkeys=1 key1=nominalshotline \
	mink1=1486 maxk1=1486 > $@1 hff=$@1@@
	Window3d max1=6.0 < $@1 > $@2
	Headermath maxsize=100000 \
	key1=offx eqn1='rx-sx' \
	< $@2 > $@ hff=$@@@;
	rm $@1 $@2

################## Apply 3-D HMO correction to the shotline ##############################

%.near: %
	Window_key synch=1 nkeys=1 key1=offset maxk1=${maxoff} < $< > $@

%.port: %
	Window_key synch=1 nkeys=1 key1=gunarray_p_s mink1=1 maxk1=1 < $< > $@	hff=$@@@

%.HMO: %
	keepbin/Hmo3d vel=1510 depth='gwdep' sz='selev' gz='gelev' sx='sx' \
	sy='sy' gx='rx' gy='ry' < $< > $@

%.stacked: %
	Cp $< $@1
	echo "hff=-1" >> $@1
	Stack3d maxsize=100000 < $@1 | Pad extend=1 n2out=${nfake} > $@


################## MAKE AN AVERAGE HMO WAVELET ##############################

average_bubble.H: rec.p
	# for nodeId in $(shell less recShort.p) ; do \
	for nodeId in $(shell less rec.p) ; do \
		echo $$nodeId ; \
		make downgoing_data$$nodeId.H.near.port.HMO.stacked ;\
	done
	Cat3d axis=2 downgoing_data*.H.near.port.HMO.stacked | Stack3d > $@

################## MAKE AND APPLY THE FILTER ##############################

2DshapingFilter.H: average_bubble.H matching_wavelet.H ${Shape} 
	echo "Computing shaping filter with poslag=${poslag}..." 
	${SINGRUN} ${Shape} < $< wave=matching_wavelet.H neglag=${neglag} poslag=${poslag} eps=0.2 > $@1;
	Window3d n2=1 < $@1 > $@
	rm $@1

%.shaped: 2DshapingFilter.H ${Conv} %
	echo "Convolving $* with filter with poslag=${poslag} and neglag=${neglag}"
	${SINGRUN} ${Conv} < $* filter=2DshapingFilter.H > $@




