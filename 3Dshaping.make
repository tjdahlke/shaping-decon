
nfake=2
maxoff=500
poslag=400
neglag=20
filternode=3

############################## MAKE THE FILTER ##############################


filter_data%.H:
	make --directory=../pz-summation ${main}/pz-summation/DOWN/down$*.H
	Cp ${main}/pz-summation/DOWN/down$*.H $@

minphase_data%.H: filter_data%.H  ${Kolmo}
	Bandpass fhi=30 < $< > $@1
	Window_key synch=1 key1=offset maxk1=${maxoff} < $@1 > $@2
	${SINGRUN} ${Kolmo} eps=0.01 < $@2 > $@
	rm $@1 $@2

data_wavelet%.H: minphase_data%.H
	Stack3d normalize=1 maxsize=1000 < $< > $@4
	echo "n2=1" >> $@5
	Pad extend=1 n2out=${nfake} < $@5 > $@
	rm $@4 $@5

fshape%.H: data_wavelet%.H matching_wavelet.H ${Shape} 
	echo "Computing shaping filter with poslag=${poslag}..." 
	${SINGRUN} ${Shape} < data_wavelet$*.H wave=matching_wavelet.H neglag=${neglag} poslag=${poslag} eps=0.2 > $@1;
	Window3d n2=1 < $@1 > $@
	rm $@1

############################## APPLY THE FILTER ##############################

${UP_SHAPED}/%.shaped: fshape${filternode}.H ${Conv}
	echo "Convolving $* with filter with poslag=${poslag} and neglag=${neglag}"
	Cp ${UP_INDIR}/$* ${UP_SHAPED}/$* datapath=./UPshaped/
	${SINGRUN} ${Conv} < ${UP_SHAPED}/$* filter=fshape${filternode}.H > $@1
	Cp $@1 $@ datapath=${project}/UPshaped/
	rm $@1

${DOWN_SHAPED}/%.shaped: fshape${filternode}.H ${Conv}
	echo "Convolving $* with filter with poslag=${poslag} and neglag=${neglag}"
	Cp ${DOWN_INDIR}/$* ${DOWN_SHAPED}/$*
	${SINGRUN} ${Conv} < ${DOWN_SHAPED}/$* filter=fshape${filternode}.H > $@1
	Cp $@1 $@ datapath=${project}/DOWNshaped/
	rm $@1

rec.p:
	make --directory=../pz-summation $@
	cp ${main}/pz-summation/rec.p $@

#-------------------------------------------------------
# RUN PZ-SUMMATION ON CLUSTER
PBS-wavelet-shaping-UP: rec.p
	python ${PY}/PBS-wavelet-shaping-UP.py rec.p ./wrk ${project} ${PY}/PBStemplate.sh

PBS-wavelet-shaping-DOWN: rec.p
	python ${PY}/PBS-wavelet-shaping-DOWN.py rec.p ./wrk ${project} ${PY}/PBStemplate.sh


#-------------------------------------------------------
# COMBINE ALL UP / DOWN GOING COMPONENTS TOGETHER
shaped_UP_all.H:
	python ${PY}/recursive_cat.py 30 $@ '${UP_SHAPED}/up*.H.shaped | sort -V'

shaped_DOWN_all.H:
	python ${PY}/recursive_cat.py 30 $@ '${DOWN_SHAPED}/down*.H.shaped | sort -V'

#-------------------------------------------------------
# MAKE AN AVERAGE DATA Wavelet
average_bubble2.H:
	for nodeId in $(shell less recShort.p) ; do \
		echo $$nodeId ; \
		make data_wavelet$$nodeId.H ;\
	done
	Cat3d data_wavelet*.H | Window3d n2=1 > $@1
	Stack3d normalize=y < $@1 > $@





