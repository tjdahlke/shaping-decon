#include "float1DReg.h"
#include "complex1DReg.h"
#include "FFT1.h"
#include <omp.h>

namespace SEP { 

class Signal1D : public float1DReg {
	
public:
	Signal1D(std::shared_ptr<hypercube> hyper) : float1DReg(hyper) { }

	std::shared_ptr<Signal1D> xcorr(const std::shared_ptr <float1DReg> x); 	// compute trace by trace

	std::shared_ptr<Signal1D> conv(const std::shared_ptr <float1DReg> x);

	void levinson(const std::shared_ptr <float1DReg> acorr, const std::shared_ptr <float1DReg> xcorr, float eps);

	std::shared_ptr<float1DReg> kolmogorov(float eps);

	std::shared_ptr<float1DReg> getFloat1D(); 	// compute trace by trace
	
};

}