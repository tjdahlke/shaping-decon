#include <vector>

namespace SEP {

class Operator 
{
public:
	Operator() { };

	virtual void forward() = 0;
	virtual void adjoint() = 0;

	void getModel();

	std::vector<double> dotTest();
	std::vector<double> linTest();

protected:
	int adj;
	int add;
};

}
