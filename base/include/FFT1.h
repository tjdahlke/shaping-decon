#ifndef FFT1_H
#define FFT1_H

#include <complexHyper.h>
#include <floatHyper.h>
#include <Operator.h>
#include <fftw3.h>

namespace SEP{

class FFT1 : public Operator {

public:
	FFT1(std::shared_ptr<floatHyper> model, std::shared_ptr<complexHyper> data);

	void forward();
	void adjoint();

	void checkDomainRange();

	~FFT1() {
		fftwf_destroy_plan(_fwd_plan);
		fftwf_destroy_plan(_adj_plan);
	};

private:
	float *_in;
	fftwf_complex *_out;
	fftwf_plan _fwd_plan;
	fftwf_plan _adj_plan;
	bool _fwd_exec, _adj_exec;
};
}

#endif