#include <FFT1.h>

using namespace SEP;

FFT1::FFT1 (std::shared_ptr<floatHyper> model, std::shared_ptr<complexHyper> data) {

	int idist, odist, istride, ostride;

	int ndim = model->getHyper()->getNdimG1();
	int n1 = model->getHyper()->getAxis(1).n;

	int howmany = 1;
	for (int i=ndim;i>1;i--) howmany *= model->getHyper()->getAxis(i).n;   

	_in = reinterpret_cast<float*>(model->getVals());
	_out = reinterpret_cast<fftwf_complex*>(data->getVals());
	
	/* 
	j-th element of k-th transform is
	out = in + j*stride + k*idist
	*/

 	idist = n1;
 	odist = n1;
 	istride = 1; 
 	ostride = 1;

 	_fwd_plan = fftwf_plan_many_dft_r2c(1, &n1, howmany,
                             _in, NULL, istride, idist,
                             _out, NULL, ostride, odist,
                             FFTW_ESTIMATE);
 	_adj_plan = fftwf_plan_many_dft_c2r(1, &n1, howmany,
                             _out, NULL, istride, idist,
                             _in, NULL, ostride, odist,
                             FFTW_ESTIMATE);

}

void FFT1::forward() {

	fftwf_execute(_fwd_plan);

	// if (_adj_scale) scale;		have to know n1 how?

	_fwd_exec = true;
}

void FFT1::adjoint() {

	fftwf_execute(_adj_plan);

	// if (_fwd_exec) {scale};

	_adj_exec = true;
}

