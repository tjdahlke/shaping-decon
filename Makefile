
include makefile.top
include 3Dshaping.make
include hmo.make
include Figures.make

#==============================================================================

first_steps:
	make setdatapath
	make docker/shaping-decon-docker-nossh.simg

#==============================================================================

setdatapath:
	echo "datapath=scratch/" > .datapath

docker/shaping-decon-docker-nossh.simg:
	./docker/build_my_environment.sh
	mv shaping-decon-docker-nossh.simg $@

complex/lib/libcomplexHyper.a:
	make --directory=complex

base/lib/libBase.a:
	make --directory=base

EXE: base/lib/libBase.a complex/lib/libcomplexHyper.a ${Kolmo} ${Shape} ${Conv}

# This prevents the binaries from being removed arbitrarily
.PRECIOUS: ${B}/%.x fshape%.H

#==============================================================================

matching_wavelet.H:
	Wavelet wavelet=ricker2 fund=15 n1=4000 d1=0.002 o1=-1 tdelay=1 | Pad extend=1 n2out=${nfake} > $@

binpar=key1='sx' ng1=200 dg1=50 og1=209030 compress_tr=0
%.binned: %
	Sort3d nkeys=1  ${binpar} < $< > $@1 hff=$@1@@ gff=$@1gff
	Stack3d normalize=y < $@1 > $@
	rm $@1 $@1@@ $@1gff

#==============================================================================
burn:
	rm -f ${B}/*
	make -C ./base burn
	make -C ./complex burn
	rm -f ${R}/*
	rm -f *.H *.h *.H@* *.h@* core.*
	
